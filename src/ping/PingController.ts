export class PingController {

    public routes() {
        return [
            {
                handler: async () => {
                    return { status: 200 };
                },
                method: "GET",
                path: "/api/v1/ping",
            },
        ];
    }

}
