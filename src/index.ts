import * as Hapi from "@hapi/hapi";
import { createConnection } from "typeorm";
import { Balance } from "./balance/Balance";
import { BalanceController } from "./balance/BalanceController";
import { PingController } from "./ping/PingController";

(async () => {
    await createConnection({
        database: "balance_sample",
        dropSchema: true,
        entities: [
            Balance,
        ],
        host: "localhost",
        logging: ["warn", "error"],
        password: "password",
        synchronize: true,
        type: "mysql",
        username: "root",
    });
    const server = new Hapi.server({
        host: "localhost",
        port: 3000,
    });
    server.route(
        [].concat.apply(
            [],
            [
                new BalanceController().routes(),
                new PingController().routes(),
            ],
        ),
    );
    await server.start();
    console.log(`Running on ${server.info.uri}`);
})();
