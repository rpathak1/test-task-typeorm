import * as Joi from "@hapi/joi";
import { Decimal } from "decimal.js";
import { BalanceService } from "./BalanceService";

export class BalanceController {

    private service: BalanceService;

    constructor() {
        this.service = new BalanceService();
    }

    public routes() {
        return [
            {
                handler: async () => {
                    return this.service.get();
                },
                method: "GET",
                path: "/api/v1/balance",
            }, {
                handler: async (request) => {
                    return this.service.insert(
                        request.params.category,
                        request.params.type,
                    );
                },
                method: "POST",
                options: {
                    validate: {
                        params: Joi.object()
                            .keys({
                                category: Joi.string()
                                    .required()
                                    .valid("NORMAL", "MERCHANT"),
                                type: Joi.string()
                                    .required()
                                    .valid("BTC", "ETH"),
                            }),
                    },
                },
                path: "/api/v1/balance/category/{category}/type/{type}",
            }, {
                handler: async (request) => {
                    return this.service.transfer(
                        request.payload.fromCategory,
                        request.payload.toCategory,
                        request.params.type,
                        new Decimal(request.payload.amount),
                    );
                },
                method: "POST",
                options: {
                    validate: {
                        payload: Joi.object()
                            .keys({
                                amount: Joi.number()
                                    .required()
                                    .positive(),
                                fromCategory: Joi.string()
                                    .required()
                                    .valid("NORMAL", "MERCHANT"),
                                toCategory: Joi.string()
                                    .required()
                                    .valid("NORMAL", "MERCHANT"),
                                type: Joi.string()
                                    .required()
                                    .valid("BTC", "ETH"),
                            }),
                    },
                },
                path: "/api/v1/balance/transfer",
            },
        ];
    }

}
