import { Decimal } from "decimal.js";
import { BalanceView } from "./BalanceView";

export class BalanceService {

    public async get(): Promise<BalanceView[]> {
        throw new Error("Not implemented");
    }

    public async insert(
        category: "NORMAL" | "MERCHANT",
        type: "BTC" | "ETH",
    ): Promise<BalanceView[]> {
        throw new Error("Not implemented");
    }

    public async transfer(
        fromCategory: "NORMAL" | "MERCHANT",
        toCategory: "NORMAL" | "MERCHANT",
        type: "BTC" | "ETH",
        amount: Decimal,
    ): Promise<BalanceView[]> {
        throw new Error("Not implemented");
    }

}
