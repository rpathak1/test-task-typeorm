import { Column, Entity, Index, PrimaryGeneratedColumn } from "typeorm";

@Entity()
@Index(["category", "type"], { unique: true })
export class Balance {

    @PrimaryGeneratedColumn()
    public id: number;

    @Column("nvarchar")
    public category: "NORMAL" | "MERCHANT";

    @Column("nvarchar")
    public type: "BTC" | "ETH";

    @Column("decimal", { precision: 20, scale: 8, default: 0 })
    public balance: string;

}
