import { EntityRepository, Repository } from "typeorm";
import { Balance } from "./Balance";

@EntityRepository()
export class BalanceRepository extends Repository<Balance> {

    public async findOneByCategoryAndTypeOrFail(
        category: "NORMAL" | "MERCHANT",
        type: "BTC" | "ETH",
    ): Promise<Balance> {
        const balance: Balance | undefined = await this.createQueryBuilder()
            .where("category = :category", { category })
            .andWhere("type = :type", { type })
            .getOne();
        if (!balance) {
            throw new Error("Not found");
        }
        return balance;
    }

}
