import { Balance } from "./Balance";
import { BalanceView } from "./BalanceView";

export class BalanceConverter {

    public async convert(balance: Balance): Promise<BalanceView> {
        throw new Error("Not implemented");
    }

    public async convertMany(balances: Balance[]): Promise<BalanceView[]> {
        throw new Error("Not implemented");
    }

}
