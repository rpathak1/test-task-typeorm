export interface BalanceView {

    category: "NORMAL" | "MERCHANT";
    type: "BTC" | "ETH";
    balance: string;

}
