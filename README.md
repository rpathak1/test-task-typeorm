# Background

Balances hold a specific cryptocurrency, as defined in `type`. They also have a category, which can be either `MERCHANT` or `NORMAL`.

# Task

Your task is to implement the methods in `BalanceService.ts`. The parameters and return types are defined in TypeScript signatures. You may have to update existing entities or create new functionality elsewhere.

- `BalanceService::get` - returns all balances as views
- `BalanceService::insert` - creates a new balance, fails fast on collision
- `BalanceService::transfer` - move coins from one balance to the other of the same type; eg. move from Bitcoin Normal balance to Bitcoin Merchant balance; make sure this method is not attackable!

# Running

The provided project skeleton is runnable as defined in `package.json`. You may configure MySQL to your liking in `index.ts`.
